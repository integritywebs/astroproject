import React from "react";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import '../scss/font.scss';
import '../scss/resets.scss';
import '../scss/App.scss';
import HomePage from '../components/HomePage';
import Contactus from '../components/contactus';
import Aboutus from '../components/aboutus';
import Blog from '../components/BlogScreen';
import Blogs from '../components/Blog';
import OurServices from '../components/OurServices';
import LandR from '../components/LoveAndRelation'
import Product from '../components/Product';
import MainProduct from '../components/MainProduct';
import Login from '../components/Login';
import Aries from '../components/Aries';
import Libra from '../components/Libra';
import Scorpio from '../components/Scorpio';
import Sagittarius from '../components/Sagittarius';
import Capricorn from '../components/Capricorn';
import Aquarius from '../components/Aquarius';
import Pisces from '../components/Pisces';
import Taurus from '../components/Taurus';
import Gemini from '../components/Gemini';
import Cancer from '../components/Cancer';
import Leo from '../components/Leo';
import Virgo from '../components/Virgo';
function App(props) {
  return (

    <Router>
      <div className="app">
        <Switch>
          <Route exact path='/' component={HomePage} />
          <Route path='/contact' component={Contactus} />
          <Route path="/aboutus" component={Aboutus} />
          <Route path="/blog" component={Blog} />
          <Route path="/blogs/:id" component={Blogs} />
          <Route path="/services" component={OurServices} />
          <Route path="/loveandrelationship/:id" component={LandR} />
          <Route path="/product/:id" component={Product} />
          <Route path="/mainproduct/:id" component={MainProduct} />
          <Route path="/adminlogin" component={Login} />
          <Route path="/Aries" component={Aries} />
          <Route path="/Virgo" component={Virgo} />
          <Route path="/Leo" component={Leo} />
          <Route path="/Cancer" component={Cancer} />
          <Route path="/Gemini" component={Gemini} />
          <Route path="/Taurus" component={Taurus} />
          <Route path="/Pisces" component={Pisces} />
          <Route path="/Aquarius" component={Aquarius} />
          <Route path="/Capricorn" component={Capricorn} />
          <Route path="/Sagittarius" component={Sagittarius} />
          <Route path="/Scorpio" component={Scorpio} />
          <Route path="/Libra" component={Libra} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
