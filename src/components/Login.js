import React,{useState} from 'react'
import axios from 'axios'
import {Form,Button} from 'react-bootstrap'
import Navbar from '../components/Navbar'
import Footer from '../components/Footer'
import '../scss/login.scss'
function Login() {
      const url="http://localhost:8080/api/signin"
    const [login,setLogin]=useState({
     email:"",
     password:""
    
   })
    function submit(e){
      // e.preventDefault();
      console.log(e)
      axios.post(url,{
        "email":login.email,
        "password":login.password
      }
     )
      .then(res=>{
        console.log(res.data)
        if(res.data.Message=="User found and successfully Logged In"){
            alert("Successfully login")
            window.open("http://localhost:2000/");
        }
      })
  }

    function handle(e){
      const newData={...login}
      newData[e.target.id]=e.target.value
      setLogin(newData)
      console.log(newData)
    }
    return (
        <div>
            <Navbar />
            <div className="login">
                <h1 className="login_heading">Admin Login</h1>
                <Form onSubmit={(e)=>submit(e)}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label className="fieldName">Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" id="email" required value={login.email} onChange={(e)=> handle(e)} />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label  className="fieldName">Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" id="password" required value={login.password} onChange={(e)=> handle(e)}/>
                        </Form.Group>
                        <Button variant="primary" onSubmit={(e)=>submit(e)}  type="submit">
                            Submit
                        </Button>
                </Form>
            </div>
            <Footer/>
        </div>
    )
}

export default Login
