import React, { useState, useEffect } from "react";
import axios from 'axios';
import '../scss/App.scss';
import '../scss/font.scss';
import divider from '../images/Line 61.png';
import ReactPlayer from 'react-player'

function Vedios() {
    const [vedio, setVedio] = useState([])
    useEffect(() => {

        axios.get('http://localhost:8080/api/latestVideo')

            .then(res => {
                console.log(res.data);
                setVedio(res.data.data);
            })
            .catch(err => {
                console.log(err);
            })
    })
    const getDate = (date) => {
        var options = { year: 'numeric', month: 'long', day: 'numeric' };
        return new Date(date).toLocaleDateString([], options);
    }
    return (
        <div className="Blogs" style={{ marginTop: '5%' }} >
            <p className="sale_text">LATEST VIDEOS</p>
            <img className="divider" src={divider} alt="divider" />
            <div className="blogs_cart">
                {vedio.map(vedios => {
                    return (
                        <div className="expert_slider">
                            <div className="expert_image">
                                <ReactPlayer
                                    url={vedios.videoUrl}
                                    controls
                                    playbackRate={2}
                                    width="300px"
                                    height="150px"
                                />
                            </div>
                            <div className="expert_cart_text">
                                <div className="expert_name">
                                    <p className="expert_text">{vedios.title}</p>
                                    <p className="expert_text">{getDate(vedios.date)}</p>
                                </div>
                            </div>
                        </div>
                    );
                })}

            </div>
        </div>
    );
}

export default Vedios;
