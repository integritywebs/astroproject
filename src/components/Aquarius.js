import React from 'react'
import '../scss/about.scss'
import '../scss/font.scss'
import '../scss/App.scss'
import '../scss/resets.scss'
import Navbar from './Navbar';
import Footer from './Footer';
import { Breadcrumb, Button } from 'react-bootstrap'
import '../scss/services.scss';
import '../scss/horoscope.scss';
import ico1 from '../images/horo/ico1.png'
import ico2 from '../images/horo/ico2.png'
import ico3 from '../images/horo/ico3.png'
import ico4 from '../images/horo/ico4.png'
import ico5 from '../images/horo/ico5.png'
import ico6 from '../images/horo/ico6.png'
import ico7 from '../images/horo/ico7.png'
import ico8 from '../images/horo/ico8.png'
import ico9 from '../images/horo/ico9.png'
import ico10 from '../images/horo/ico10.png'
import ico11 from '../images/horo/ico11.png'
import ico12 from '../images/horo/ico12.png'

const Aquarius = () => {
    return (
        <div className="AriesWrapper">
            <Navbar />
            <div className="header">
                <Breadcrumb className="bread">
                    <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                    <Breadcrumb.Item active>Aquarius</Breadcrumb.Item>
                </Breadcrumb>
            </div>
            <div className="Aries">
                <div className="AriesLeft">
                    <Button variant="primary" className="btn">Daily</Button>{' '}
                    <Button variant="success" className="btn">Weekly</Button>{' '}
                    <Button variant="dark" className="btn">Monthly</Button>{' '}
                    <Button variant="danger" className="btn">Yearly</Button>
                    <div className="AriesHead">
                        <h1 className="AriesHeading">About Sunsign</h1>
                        <div className="AriesDesc">
                            <h4 className="head">Aquarius ( 20 January – 18 February )</h4>
                            <p className="AriesPara">Aquarius is a sign of a water pitcher. People born under this sign are highly vulnerable and sensitive. People born under this sign are surrounded by people but they have rarely close friend and acquaintances. The people born under this sign are strong and attractive. Aquarius people are highly intellectual with a great humor.Aquarius Zodiac people are straight forward. These people enjoy the great company of their loved ones. In their personal life they tend for a peaceful life. The people of this sign hardly attach emotionally to someone. Aquarius is curious by nature and take interest in everything around them.Although some people mistaken it to be a watery sign but basically it is an airy sign. They have a great mind and a quick thinker. People born under this sign have great command over different language. The most important characteristics of Aquarius sun sign are witty, clever, creative, obstinate, emotional, rebellious, creative and caring.</p>
                            <p className="AriesPara">Aquarius positive qualities are they have a born natural intelligence. If they want they can make natural wonders around themselves. Aquarians are honest and they have a great intuition. They are liberal and don’t follow a conventional way in their life.</p>
                            <p className="AriesPara">Aquarians are sometimes found lonely, because they are simply not open at all. They found it hard to mingle with other. They find it hard to open up with people.Aquarians strengths are witty, intelligence, caring, creative and ingenious. Weaknesses of this sun sign are stubborn, unemotional, defiant, distant, and sardonic. Sometimes they can become uncaring to others. Aquarians sometimes may get aloof with the family members.</p>
                            <p className="AriesPara">Aquarians are creative and imaginative. Aquarius career is highly stimulated by the creative. They can become great discoverer, scientist, and researchers. Due to creative bent of mind they can also do well in acting, theater, dramatist and musician.Romantic relation with Aquarius can be strenuous for the people who are emotional. Aquarius women have sensual and feminine instinct. They have a beautiful appearance. Ruling planet for Aquarius sign is Saturn.Birthstone for this sun sign is Amethyst. The favorable month for Aquarians are March, April, May, July, August, October, November and December. Aquarius people may suffer from the disease of legs and ankle.</p>
                        </div>
                    </div>
                </div>
                <div className="AriesRight">
                    <div className="RightDiv">
                        <a href="/Aries"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico1} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%', marginTop: '10px' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '30px' }}>Aries (Mar 21 - Apr 19)</p>
                        </div></a>
                        <hr />
                        <a href="/Taurus"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico2} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}> Taurus (Apr 20 - May 20)</p>
                        </div></a>
                        <hr />
                        <a href="/Gemini"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico3} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Gemini (May 21 - Jun 20)</p>
                        </div></a>
                        <hr />
                        <a href="/Cancer"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico4} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Cancer (Jun 21 - Jul 22)</p>
                        </div></a>
                        <hr />
                        <a href="/Leo"><div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico5} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Leo (Jul 23 - Aug 22)</p>
                        </div></a>
                        <hr />
                        <a href="/Virgo"><div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico6} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Virgo (Aug 23 - Sep 22)</p>
                        </div></a>
                        <hr />
                        <a href="/Libra"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico7} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Libra (Sep 23 - Oct 22)</p>
                        </div></a>
                        <hr />
                        <a href="/Scorpio"><div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico8} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Scorpio (Oct 23 - Nov 21)</p>
                        </div></a>
                        <hr />
                        <a href="/Sagittarius"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico9} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}> Sagittarius (Nov 22 - Dec 21)</p>
                        </div></a>
                        <hr />
                        <a href="/Capricorn"><div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico10} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Capricorn (Dec 22 - Jan 19)</p>
                        </div></a>
                        <hr />
                        <a href="/Aquarius"><div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico11} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}> Aquarius (Jan 20 - Feb 18)</p>
                        </div></a>
                        <hr />
                        <a href="/Pisces"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center', textAlign: 'center' }}>
                            <img src={ico12} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%', marginBottom: '30px' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginBottom: '30px', marginTop: '10px' }}>Pisces (Feb 19 - Mar 20)</p>
                        </div></a>
                    </div>

                </div>

            </div>
            <Footer />
        </div>
    );
}
export default Aquarius;