import React from 'react'
import '../scss/about.scss'
import '../scss/font.scss'
import '../scss/App.scss'
import '../scss/resets.scss'
import Navbar from './Navbar';
import Footer from './Footer';
import { Breadcrumb, Button } from 'react-bootstrap'
import '../scss/services.scss';
import '../scss/horoscope.scss';
import ico1 from '../images/horo/ico1.png'
import ico2 from '../images/horo/ico2.png'
import ico3 from '../images/horo/ico3.png'
import ico4 from '../images/horo/ico4.png'
import ico5 from '../images/horo/ico5.png'
import ico6 from '../images/horo/ico6.png'
import ico7 from '../images/horo/ico7.png'
import ico8 from '../images/horo/ico8.png'
import ico9 from '../images/horo/ico9.png'
import ico10 from '../images/horo/ico10.png'
import ico11 from '../images/horo/ico11.png'
import ico12 from '../images/horo/ico12.png'

const Capricorn = () => {
    return (
        <div className="AriesWrapper">
            <Navbar />
            <div className="header">
                <Breadcrumb className="bread">
                    <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                    <Breadcrumb.Item active>Capricorn</Breadcrumb.Item>
                </Breadcrumb>
            </div>
            <div className="Aries">
                <div className="AriesLeft">
                    <Button variant="primary" className="btn">Daily</Button>{' '}
                    <Button variant="success" className="btn">Weekly</Button>{' '}
                    <Button variant="dark" className="btn">Monthly</Button>{' '}
                    <Button variant="danger" className="btn">Yearly</Button>
                    <div className="AriesHead">
                        <h1 className="AriesHeading">About Sunsign</h1>
                        <div className="AriesDesc">
                            <h4 className="head">Capricorn ( 22 December – 20 January )</h4>
                            <p className="AriesPara">Capricorns are self assured person. They are risk takers but also a reckless people. People of Capricorn sun sign can draw comfort from their knowledge. They have a complex relationship with the world. They can become selfish for fulfilling their own desire. Capricorn has a strong desire to gain enough leadership in the professional and personal front. These people are go getters, they do not believe that opportunity will knock at your door, but believe to create their own door of success. They have a tender heart beneath their hard shell. Capricorn falls under the sign of Earth. Astrological signs which fall under the sign of earth are very practical individuals. All their plans are preplanned and they rather face any failure in their life. They carry perfection to achieve success. Capricorn nature is marked by the sign of goat. This shows the great ambition of this sign. Capricorn key planet is Saturn. This planet symbolizes boundaries and limitations. Saturn indicates that you get what you deserve. If you try to take short cuts in your life then it may cause harm. The most prominent characteristics of Capricorn Zodiac sign are responsible, tolerant, ambitious, resourceful, trustworthy, proud, doubtful and boring. The positive traits of Capricorn people are responsible. These people are very responsible towards their family members. They are socialized people love their friends and relations. They are reliable and humble friends. The negative traits of this people are that they lack direction in their life. They are useless dominated people and do not like to get any direction from the other. On the one hand they are resourceful but on the other front they are doubtful. Capricorn strengths are dependable, patient, determined, loyal, and practical. Capricorn’s weaknesses are autocratic, reserve, vain, suspicious and unimaginative. Capricorn’s career is determined by the ambition and hard work. They are focused towards their goal and try hard to achieve it. They try again and again until they succeed in their career. In their romantic life this sun sign is loyal and committed towards their partner. Capricorns women make great mothers and love in raising the family. Capricorns men are passionate but very emotional. Lucky color for Capricorn sign is White, Cream, Dark brown, Prussian blue and Crimson. Birthstone for Capricorn is garnet. Ruling planet for this sun sign is Saturn. Health ailments of Capricorn include all the skin diseases.</p>
                        </div>
                    </div>
                </div>
                <div className="AriesRight">
                    <div className="RightDiv">
                        <a href="/Aries"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico1} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%', marginTop: '10px' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '30px' }}>Aries (Mar 21 - Apr 19)</p>
                        </div></a>
                        <hr />
                        <a href="/Taurus"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico2} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}> Taurus (Apr 20 - May 20)</p>
                        </div></a>
                        <hr />
                        <a href="/Gemini"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico3} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Gemini (May 21 - Jun 20)</p>
                        </div></a>
                        <hr />
                        <a href="/Cancer"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico4} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Cancer (Jun 21 - Jul 22)</p>
                        </div></a>
                        <hr />
                        <a href="/Leo"><div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico5} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Leo (Jul 23 - Aug 22)</p>
                        </div></a>
                        <hr />
                        <a href="/Virgo"><div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico6} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Virgo (Aug 23 - Sep 22)</p>
                        </div></a>
                        <hr />
                        <a href="/Libra"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico7} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Libra (Sep 23 - Oct 22)</p>
                        </div></a>
                        <hr />
                        <a href="/Scorpio"><div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico8} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Scorpio (Oct 23 - Nov 21)</p>
                        </div></a>
                        <hr />
                        <a href="/Sagittarius"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico9} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}> Sagittarius (Nov 22 - Dec 21)</p>
                        </div></a>
                        <hr />
                        <a href="/Capricorn"><div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico10} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Capricorn (Dec 22 - Jan 19)</p>
                        </div></a>
                        <hr />
                        <a href="/Aquarius"><div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico11} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}> Aquarius (Jan 20 - Feb 18)</p>
                        </div></a>
                        <hr />
                        <a href="/Pisces"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center', textAlign: 'center' }}>
                            <img src={ico12} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%', marginBottom: '30px' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginBottom: '30px', marginTop: '10px' }}>Pisces (Feb 19 - Mar 20)</p>
                        </div></a>
                    </div>

                </div>

            </div>
            <Footer />
        </div>
    );
}
export default Capricorn;