import React from 'react'
import '../scss/about.scss'
import '../scss/font.scss'
import '../scss/App.scss'
import '../scss/resets.scss'
import Navbar from './Navbar';
import Footer from './Footer';
import { Breadcrumb, Button } from 'react-bootstrap'
import '../scss/services.scss';
import '../scss/horoscope.scss';
import ico1 from '../images/horo/ico1.png'
import ico2 from '../images/horo/ico2.png'
import ico3 from '../images/horo/ico3.png'
import ico4 from '../images/horo/ico4.png'
import ico5 from '../images/horo/ico5.png'
import ico6 from '../images/horo/ico6.png'
import ico7 from '../images/horo/ico7.png'
import ico8 from '../images/horo/ico8.png'
import ico9 from '../images/horo/ico9.png'
import ico10 from '../images/horo/ico10.png'
import ico11 from '../images/horo/ico11.png'
import ico12 from '../images/horo/ico12.png'

const Virgo = () => {
    return (
        <div className="AriesWrapper">
            <Navbar />
            <div className="header">
                <Breadcrumb className="bread">
                    <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                    <Breadcrumb.Item active>Virgo</Breadcrumb.Item>
                </Breadcrumb>
            </div>
            <div className="Aries">
                <div className="AriesLeft">
                    <Button variant="primary" className="btn">Daily</Button>{' '}
                    <Button variant="success" className="btn">Weekly</Button>{' '}
                    <Button variant="dark" className="btn">Monthly</Button>{' '}
                    <Button variant="danger" className="btn">Yearly</Button>
                    <div className="AriesHead">
                        <h1 className="AriesHeading">About Sunsign</h1>
                        <div className="AriesDesc">
                            <h4 className="head">Virgo ( 23 August – 23 September )</h4>
                            <p className="AriesPara"> Virgo Zodiac is a sign of virginity which shows purity. This is a very independent sign. Virgo fully put their intelligence to get best in their life. But due to their narrow-mindedness, their creativity suffers sometimes. Virgo may not be totally extrovert, but they are fully capable to make their social group. Virgo is associated with the planet Mercury which denotes language and the way you express them. Mercury is also about the intellectual discrimination, which helps to shift the good to bad. This is the key planet of intellectual thought and helps to take judgmental decision regarding every bit of information which enters the mind.</p>
                            <p className="AriesPara">Virgo sometimes may dwell the past too much which can make them confused sometimes. People look to Virgo’s thinker because they are straight thinker and can solve the problems logically. Some people may find them cold and detached because they live in their own thoughts. It is better to keep yourself emotionally away from Virgo, unless and until they open up to you first. Be patient with the Virgo friend as they love patience. Virgos are very good at problem solving ability. They are rational thinkers and put each part of the problems into several pieces before solving them. They are rational thinkers. They have an excellent and a highly analytical mind, this makes them good investigators and researchers.</p>
                            <p className="AriesPara">The most prominent trait of a Virgo is modesty, reliable, practical, analytical, intelligent, and meticulous. The negative traits of a Virgo are overcritical, fussy and sometimes harsh. Virgos are sometimes conservative and won’t expect the change. The Virgo born are sometimes criticize for being judgmental. They cannot ignore false and negligence and anything less than perfect is not good for them. Virgos are loyal partner. They need intellectual stimulation if you want to win their hearts. They are sometimes conventional people so do not force them, as it can lead to a broken relationship. They are not slow, their brains continuously works and evaluate things. To get the Virgo love, be patient in your attitude.Know your partner The lucky gemstones for Virgo are Emerald and Diamond. The lucky color for them is all shades of green and blue.</p>
                        </div>
                    </div>
                </div>
                <div className="AriesRight">
                    <div className="RightDiv">
                        <a href="/Aries"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico1} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%', marginTop: '10px' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '30px' }}>Aries (Mar 21 - Apr 19)</p>
                        </div></a>
                        <hr />
                        <a href="/Taurus"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico2} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}> Taurus (Apr 20 - May 20)</p>
                        </div></a>
                        <hr />
                        <a href="/Gemini"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico3} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Gemini (May 21 - Jun 20)</p>
                        </div></a>
                        <hr />
                        <a href="/Cancer"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico4} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Cancer (Jun 21 - Jul 22)</p>
                        </div></a>
                        <hr />
                        <a href="/Leo"><div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico5} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Leo (Jul 23 - Aug 22)</p>
                        </div></a>
                        <hr />
                        <a href="/Virgo"><div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico6} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Virgo (Aug 23 - Sep 22)</p>
                        </div></a>
                        <hr />
                        <a href="/Libra"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico7} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Libra (Sep 23 - Oct 22)</p>
                        </div></a>
                        <hr />
                        <a href="/Scorpio"><div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico8} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Scorpio (Oct 23 - Nov 21)</p>
                        </div></a>
                        <hr />
                        <a href="/Sagittarius"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico9} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}> Sagittarius (Nov 22 - Dec 21)</p>
                        </div></a>
                        <hr />
                        <a href="/Capricorn"><div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico10} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Capricorn (Dec 22 - Jan 19)</p>
                        </div></a>
                        <hr />
                        <a href="/Aquarius"><div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico11} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}> Aquarius (Jan 20 - Feb 18)</p>
                        </div></a>
                        <hr />
                        <a href="/Pisces"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center', textAlign: 'center' }}>
                            <img src={ico12} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%', marginBottom: '30px' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginBottom: '30px', marginTop: '10px' }}>Pisces (Feb 19 - Mar 20)</p>
                        </div></a>
                    </div>

                </div>

            </div>
            <Footer />
        </div>
    );
}
export default Virgo;