import React, { useState } from 'react';
import axios from 'axios';
import '../scss/App.scss';
import '../scss/font.scss';
import { Card, Button, Form, Col, InputGroup, FormControl, Modal, Breadcrumb } from 'react-bootstrap'
import divider from '../images/Line 61.png';
import Slider from "react-slick";
import expert1 from '../images/expert1.jpg';
import expert2 from '../images/expert2.jpg';
import expert3 from '../images/expert3.jpg';
import expert4 from '../images/expert4.png';
import icon1 from '../images/Vector (3).png';
import icon2 from '../images/Vector (4).png';
import icon3 from '../images/Vector(5).png';

function Experts() {
  const url = "http://localhost:8080/api/consultation"
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);

  const handleShow = () => setShow(true);

  const [consultent, setConsultent] = useState({
    birthHour: "",
    birthPlace: "",
    dob: "",
    option1: "",
    mobileNumber: "",
    email: "",
    name: ""
  })
  function submit(e) {
    e.preventDefault();
    console.log(e)
    axios.post(url, {
      "birthHour": consultent.birthHour,
      "birthPlace": consultent.birthPlace,
      "dob": consultent.dob,
      "option1": consultent.option1,
      "mobileNumber": consultent.mobileNumber,
      "email": consultent.email,
      "name": consultent.name
    }
    )
      .then(res => {
        console.log(res.data)
        alert("Successfully inserted");
        handleClose();
      })
  }

  function handle(e) {
    const newData = { ...consultent }
    newData[e.target.id] = e.target.value
    setConsultent(newData)
    console.log(newData)
  }

  var settings = {
    dots: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      }

    ]
  };
  return (
    <div className="Experts">
      <div className="our_services" style={{ marginTop: '10%' }}>
        <p className="our_services_text">SPEAK TO OUR EXPERTS</p>
        <img className="divider" src={divider} alt="divider" />
      </div>
      <p className="text">Get in touch with our experts to avail specified consulting and guidance, be it in Match Making, </p>
      <p className="text1"> Bollywood Astrology, Vedic Astrology, Medical Astrology, Career Astrology, Palmistry or Gemology, it is <p className="text2">just a mail or call away.</p> </p>
      <div className="horoscope_carousel">
        <Slider {...settings} className="slider">

          <div className="horoscopeWrapper">
            <img className="horoscope_image" src={expert1} alt="horoscope" />
            <p className="expert_name" >Vipin Kapoor</p>
            <p className="professional_name">Astrologer</p>
            <div className="expert_social">
              <img className="icons" src={icon1} alt="messages" />
              <img className="icons" src={icon2} alt="messages" />
              <img className="icons" src={icon3} alt="messages" />
            </div>
          </div>

          <div className="horoscopeWrapper">
            <img className="horoscope_image" src={expert2} alt="horoscope" />
            <p className="horoscope_text" >Prashant Kapoor</p>
            <p className="professional_name">Astrologer</p>
            <div className="expert_social">
              <img className="icons" src={icon1} alt="messages" />
              <img className="icons" src={icon2} alt="messages" />
              <img className="icons" src={icon3} alt="messages" />
            </div>
          </div>

          <div className="horoscopeWrapper">
            <img className="horoscope_image" src={expert3} alt="horoscope" />
            <p className="horoscope_text" >Satarupa Kapoor</p>
            <p className="professional_name">Gemologist</p>
            <div className="expert_social">
              <img className="icons" src={icon1} alt="messages" />
              <img className="icons" src={icon2} alt="messages" />
              <img className="icons" src={icon3} alt="messages" />
            </div>
          </div>

          <div className="horoscopeWrapper">
            <img className="horoscope_image" src={expert4} alt="horoscope" />
            <p className="horoscope_text" >Akansha Srivastava</p>
            <p className="professional_name">Numerologist</p>
            <div className="expert_social">
              <img className="icons" src={icon1} alt="messages" />
              <img className="icons" src={icon2} alt="messages" />
              <img className="icons" src={icon3} alt="messages" />
            </div>
          </div>

          <div className="horoscopeWrapper">
            <img className="horoscope_image" src={expert1} alt="horoscope" />
            <p className="expert_name" >Vipin Kapoor</p>
            <p className="professional_name">Astrologer</p>
            <div className="expert_social">
              <img className="icons" src={icon1} alt="messages" />
              <img className="icons" src={icon2} alt="messages" />
              <img className="icons" src={icon3} alt="messages" />
            </div>
          </div>

          <div className="horoscopeWrapper">
            <img className="horoscope_image" src={expert2} alt="horoscope" />
            <p className="horoscope_text" >Prashant Kapoor</p>
            <p className="professional_name">Astrologer</p>
            <div className="expert_social">
              <img className="icons" src={icon1} alt="messages" />
              <img className="icons" src={icon2} alt="messages" />
              <img className="icons" src={icon3} alt="messages" />
            </div>
          </div>

          <div className="horoscopeWrapper">
            <img className="horoscope_image" src={expert3} alt="horoscope" />
            <p className="horoscope_text" >Satarupa Kapoor</p>
            <p className="professional_name">Gemologist</p>
            <div className="expert_social">
              <img className="icons" src={icon1} alt="messages" />
              <img className="icons" src={icon2} alt="messages" />
              <img className="icons" src={icon3} alt="messages" />
            </div>
          </div>

        </Slider>

      </div>
      <center> <button className="view_collection_text" onClick={handleShow} style={{ marginTop: '5%' }}> GET FREE CONSULTATION</button></center>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Get Free Consultent</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Form onSubmit={(e) => submit(e)}>
            <Form.Group>
              <Form.Control type="text" placeholder="Name" id="name" required value={consultent.name} onChange={(e) => handle(e)} />
            </Form.Group>
            <Form.Group>
              <Form.Control type="email" required placeholder="Email" id="email" value={consultent.email} onChange={(e) => handle(e)} />
            </Form.Group>
            <Form.Group>
              <Form.Control type="text" required placeholder="Mobile No." id="mobileNumber" value={consultent.mobileNumber} onChange={(e) => handle(e)} />
            </Form.Group>
            <Form.Group>
              <Form.Control type="text" required placeholder="Gender" id="option1" value={consultent.option1} onChange={(e) => handle(e)} />
            </Form.Group>
            <Form.Group>
              <Form.Control type="text" required placeholder="Date of Birth" id="dob" value={consultent.dob} onChange={(e) => handle(e)} />
            </Form.Group>
            <Form.Group>
              <Form.Control type="text" required placeholder="Birth Place" id="birthPlace" value={consultent.birthPlace} onChange={(e) => handle(e)} />
            </Form.Group>
            <Form.Group>
              <Form.Control type="text" required placeholder="Birth Hour" id="birthHour" value={consultent.birthHour} onChange={(e) => handle(e)} />
            </Form.Group>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onSubmit={(e) => submit(e)} type="submit">
              Submit
            </Button>
          </Form>
        </Modal.Body>
        <Modal.Footer>

        </Modal.Footer>

      </Modal>


    </div>

  );
}

export default Experts;
